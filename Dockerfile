FROM golang:1.18.2-bullseye as builder

ARG TARGETOS
ARG TARGETARCH

ENV GOOS="${TARGETOS}"
ENV GOARCH="${TARGETARCH}"

COPY . /build
COPY ./assets /application/assets

RUN cd /build && \
    go get ./... && \
    go build -o "wakeword2mqtt" -ldflags "-s -w" . && \
    /build/build/artifacts.sh "/build/wakeword2mqtt"

# ----- main image ----

FROM alpine:latest

WORKDIR /
# Our own dependencies
COPY --from=builder /artifacts /
COPY --from=builder /usr/share/zoneinfo/${TZ} /usr/share/zoneinfo/${TZ}

WORKDIR /application
COPY --from=builder /build/wakeword2mqtt ./source

ENV MQTT_HOST="192.168.1.104"

USER 1001

ENTRYPOINT [ "./source" ] 
