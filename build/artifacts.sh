#!/bin/sh

mkdir -p /artifacts

target="${1}"
temp="/artifacts.txt"

ldd -v "${target}" | grep so | sed -e '/^[^\t]/ d' | sed -e 's/\t//' | sed -e 's/.*=..//' | sed -e 's/ (0.*)//' | sed -e '/linux-vdso/d' | tr -d ':' | sort | uniq | sort -n > ${temp}

# Some processes may need the ca-certificates to verify upastream tls connections
echo "/etc/ca-certificates.conf" >> ${temp}
echo "/etc/ssl/certs/ca-certificates.crt" >> ${temp}
echo "/usr/share/ca-certificates" >> ${temp}
echo "/application/assets" >> ${temp}

tar zcvf /artifacts.tar.gz -C $(dirname "${target}") -h -T ${temp} 2>/dev/null
mv /artifacts.tar.gz /artifacts
cd /artifacts
tar xf artifacts.tar.gz
rm -f artifacts.tar.gz
