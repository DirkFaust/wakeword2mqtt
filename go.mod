module gitlab.com/DirkFaust/wakeword2mqtt

go 1.18

require (
	github.com/Picovoice/porcupine/binding/go/v2 v2.1.3
	github.com/joho/godotenv v1.4.0
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/DirkFaust/faustility v0.0.0-20220628113750-2d7ac3ef927a
)

require (
	github.com/goiiot/libmqtt v0.9.6 // indirect
	github.com/klauspost/compress v1.15.6 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
