package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"time"

	poc "github.com/Picovoice/porcupine/binding/go/v2"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/DirkFaust/faustility/pkg/audiocrypt"
	"gitlab.com/DirkFaust/faustility/pkg/conversion"
	"gitlab.com/DirkFaust/faustility/pkg/env"
	"gitlab.com/DirkFaust/faustility/pkg/mqtt"
)

const (
	envLogLevel         = "LOG_LEVEL"
	envAudioDeviceIndex = "AUDIO_DEVICE_INDEX"
	envPassword         = "ENCRYPTION_PASSWORD"
	envAPIKey           = "API_KEY"
	envWakeword         = "WAKEWORD"
	envModelPath        = "MODEL_PATH"
	envKeywordPath      = "KEYWORD_PATH"
	envSensitivity      = "WAKEWORD_SENSITIVITY"
)

type WakeWordMessage struct {
	Timestamp int64  `json:"timestamp"`
	Word      string `json:"word"`
}

func main() {
	_ = godotenv.Load()

	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(env.GetOrDefault(envLogLevel, "info"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	var audioDeviceIndex int
	conversion.FromStringOrDie(env.GetOrDie(envAudioDeviceIndex), &audioDeviceIndex)

	var sensitivity float32
	conversion.FromStringOrDie(env.GetOrDefault(envSensitivity, "0"), &sensitivity)

	modelPath := os.Getenv(envModelPath)
	if len(modelPath) > 0 {
		log.Infof("Using custom model '%s'", modelPath)
	}

	wakeword := env.GetOrDie(envWakeword)
	var keywordPaths []string
	var builtinKeywords []poc.BuiltInKeyword
	keywordPath := os.Getenv(envKeywordPath)
	if len(keywordPath) > 0 {
		keywordPaths = append(keywordPaths, keywordPath)
		log.Infof("Using custom keyword from '%s'", keywordPath)
	} else {
		log.Infof("Using built-in keyword '%s'", wakeword)
		builtinKeywords = append(builtinKeywords, poc.BuiltInKeyword(wakeword))
	}

	porcupine := poc.Porcupine{
		AccessKey:       env.GetOrDie(envAPIKey),
		ModelPath:       modelPath,
		KeywordPaths:    keywordPaths,
		Sensitivities:   []float32{sensitivity},
		BuiltInKeywords: builtinKeywords,
	}
	err = porcupine.Init()
	if err != nil {
		panic(err)
	}

	client := mqtt.NewClient(nil, fmt.Sprintf("audio2mqtt/pcm/%d", audioDeviceIndex))

	password := os.Getenv(envPassword)
	doEncrypt := (len(password) > 0)
	var decryptor audiocrypt.AudioDecryptor

	if !doEncrypt {
		log.Warn("NOT using encryption - its highly unadviced to send your permanent(!) recorded(!) audio unencrypted. Even if the MQTT is secured via TLS, EVERY client on that server can receive AND HEAR the audio.")
	} else {
		decryptor = audiocrypt.NewAudioDecryptor(password)
	}

	all := 0
	ticker := time.NewTicker(time.Second * 1)

	wg := sync.WaitGroup{}
	wg.Add(1)

	audio := client.Generate()

	go func() {
		signalCh := make(chan os.Signal, 1)
		signal.Notify(signalCh, os.Interrupt)
		defer close(signalCh)
		defer client.Close()

	receiveLoop:
		for {
			select {
			case <-signalCh:
				log.Infof("Interrupt requested")
				break receiveLoop
			case <-ticker.C:
				log.Debugf("Audio: %.2f kbit/s", float64(all*8)/1024)
				all = 0
			case data := <-audio:
				all += len(data.Payload)
				pcm, err := decryptor.Decrypt(data.Payload)
				if err != nil {
					log.Error(err)
					break receiveLoop
				}

				keywordIndex, err := porcupine.Process(pcm)
				if err != nil {
					log.Fatal(err)
				}
				if keywordIndex >= 0 {
					log.Infof("Keyword %d detected\n", keywordIndex)

					out, err := json.Marshal(WakeWordMessage{
						Timestamp: time.Now().Unix(),
						Word:      wakeword,
					})

					if err != nil {
						log.Error(err)
						continue
					}

					client.Publish(mqtt.Message{
						Topic:   fmt.Sprintf("wakeword2mqtt/detection/%d", audioDeviceIndex),
						Payload: out,
					}, false)
				}
			}
		}
		wg.Done()
	}()

	wg.Wait()
	log.Info("Terminated")
}
